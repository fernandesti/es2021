export const calculoImc = ({ altura, peso}) => {
               return  parseFloat(peso / (altura * altura)).toFixed(2);
}